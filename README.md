# BL3 SRIM simulations

## Installing Wine

### Installing system-wide (if you have sudo access)

Start by ensuring `wine` is installed:
```
sudo apt install wine-development
winecfg
```
With `winecfg` you can set your settings, but here it mainly serves to
ensure that the `.wine` directory is created properly before we proceed.

The following commands will install the appropriate `winetricks` components:
```
sudo apt install cabextract winetricks
```

### Installing locally (if you are user)

Winetricks can be installed locally if it cannot be installed system-wide with:
```
cd ~/bin
wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
chmod a+x winetricks
export PATH=$HOME/bin:$PATH
```

Winetricks requires cabextract to function. If this cannot be installed system-wide, install with:
```
wget https://www.cabextract.org.uk/cabextract-1.9.1.tar.gz
tar -xvf cabextract-1.9.1.tar.gz
cd cabextract-1.9.1
./configure --prefix=$HOME && make && make install
```

## Installing Wine components

To be able to run SRIM, the following components are required:
```
winetricks --country=US corefonts vb5run richtx32 tabctl32 comctl32 comctl32ocx comdlg32ocx msflxgrd oleaut32
```
The `country=US` flag tries, often in vain, to make sure you do not get international license agreements.

## Installing SRIM

Download, rename, and extract the SRIM package from srim.org:
```
mkdir $HOME/SRIM-2013 && cd $HOME/SRIM-2013
wget http://srim.org/SRIM/SRIM-2013-Std.e
mv SRIM-2013-Std.e SRIM-2013-Std.exe
wine SRIM-2013-Std.exe
```
Just accept the default location (extracting in the current directory).

### Running SRIM

You should now be able to start SRIM:
```
wine $HOME/SRIM-2013/SRIM.exe
```

## Running the BL3 SRIM simulations interactively

Copy only the TRIM.IN file in this repository into the SRIM directory, with the correct Windows line endings:
```
unix2dos TRIM*
cp TRIM.IN ~/SRIM-2013
```
Now run TIN directly from the SRIM directory:
```
cd ~/SRIM-2013
wine TIN.exe
```
- Click 'Restore Last TRIM Data' in the top left, which will load the right layer configuration.
- Modify the settings as needed.
- Click 'Save Input & Run TRIM' in the bottom right to run the simulation.

## Running the BL3 SRIM simulations in batch mode

Copy all files in this repository into the SRIM directory, with the correct Windows line endings:
```
unix2dos TRIM*
cp TRIM* ~/SRIM-2013
```
Now run TRIM directly from the SRIM directory:
```
cd ~/SRIM-2013
wine TRIM.exe
```
This will automatically run in batch mode, without graphics.

## Analyzing the results

This will generate a number of txt files:
- SRIM Restore/*.sav: data to continue the simulation (see TRIMAUTO)
- SRIM Outputs/BACKSCAT.txt: backscattered protons
- SRIM Outputs/TRIMOUT.txt: transmitted protons
- RANGE.txt: proton range histogram
- IONIZ.txt: proton ionization histogram

